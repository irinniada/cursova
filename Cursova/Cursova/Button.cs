﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Cursova
{
    class Button
    {
        //many weird characteristics...
        private int iXPos, iYPos;
        private int iWidth, iHeight;
        private int imgID;

        private Boolean clickable;

        //add constructor...
        public Button(int iXPos, int iYPos, int iWidth, int iHeight, int imgID, Boolean clickable)
        {
            this.iXPos = iXPos;
            this.iYPos = iYPos;
            this.iWidth = iWidth;
            this.iHeight = iHeight;
            this.imgID = imgID;
            this.clickable = clickable;
        }

        //it must draw Button, not Game!!!
        public void Draw(Graphics g, Bitmap oB)
        {
            g.DrawImage(oB, new Point(iXPos, iYPos));
        }

        //why I write it?.. well, let it be
        public int getXPos()
        {
            return iXPos;
        }

        public int getYPos()
        {
            return iYPos;
        }

        public int getWidth()
        {
            return iWidth;
        }

        public int getHeight()
        {
            return iHeight;
        }

        public int getIMGID()
        {
            return imgID;
        }

        public Boolean getClickable()
        {
            return clickable;
        }
    }
}
