﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cursova
{
    public partial class StartPage : Form
    {
        private Game oGame;

        private Graphics gGraphics, gG;
        private Bitmap bBackground;

        //many weird letters...
        public StartPage()
        {
            InitializeComponent();

            bBackground = new Bitmap(396, 600);

            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.StartPosition = FormStartPosition.CenterScreen;
            gGraphics = this.CreateGraphics();
            gG = Graphics.FromImage(bBackground);

            oGame = new Game();
            
        }

        public void UpdateGame()
        {
            oGame.Update();
           
        }

        public void Draw(Graphics g)
        {
            g.Clear(Color.FromArgb(251, 248, 239));

            oGame.Draw(g);
        }

        //timer needed only to start. I didn't remember, why I choosed THIS method o_0
        private void timer1_Tick(object sender, EventArgs e)
        {
            //random starting location...
            UpdateGame();
            //...and if we can draw...
            if (oGame.bRender)
            {
                //...we draw!!!
                Draw(gG);

                gGraphics.DrawImage(bBackground, new Point(0, 0));
            }
        }

        //to press or not to press - that is the question
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (!oGame.kTOP && !oGame.kRIGHT && !oGame.kBOTTOM && (e.KeyCode == Keys.A || e.KeyCode == Keys.Left))
            {
                oGame.kLEFT = true;
                oGame.moveBoard(Game.Direction.eLEFT);
            }
            else if (!oGame.kLEFT && !oGame.kRIGHT && !oGame.kBOTTOM && (e.KeyCode == Keys.W || e.KeyCode == Keys.Up))
            {
                oGame.kTOP = true;
                oGame.moveBoard(Game.Direction.eTOP);
            }
            else if (!oGame.kTOP && !oGame.kLEFT && !oGame.kBOTTOM && (e.KeyCode == Keys.D || e.KeyCode == Keys.Right))
            {
                oGame.kRIGHT = true;
                oGame.moveBoard(Game.Direction.eRIGHT);
            }
            else if (!oGame.kTOP && !oGame.kRIGHT && !oGame.kLEFT && (e.KeyCode == Keys.S || e.KeyCode == Keys.Down))
            {
                oGame.kBOTTOM = true;
                oGame.moveBoard(Game.Direction.eBOTTOM);
            }
            //and the small code to reload game after gameover
            else if (Game.gameOver == true && e.KeyCode == Keys.Enter)
            {
                oGame.resetGameData();
               
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (oGame.kLEFT && (e.KeyCode == Keys.A || e.KeyCode == Keys.Left))
            {
                oGame.kLEFT = false;
            }

            if (oGame.kTOP && (e.KeyCode == Keys.W || e.KeyCode == Keys.Up))
            {
                oGame.kTOP = false;
            }

            if (oGame.kRIGHT && (e.KeyCode == Keys.D || e.KeyCode == Keys.Right))
            {
                oGame.kRIGHT = false;
            }

            if (oGame.kBOTTOM && (e.KeyCode == Keys.S || e.KeyCode == Keys.Down))
            {
                oGame.kBOTTOM = false;
            }
        }

        //wait! we have a buttons to click!
        private void Main_MouseClick(object sender, MouseEventArgs e)
        {
            oGame.checkButton(e.X, e.Y);
        }

     }
}
